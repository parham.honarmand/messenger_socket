package connection_manager;

import java.sql.Connection;
import java.sql.DriverManager;

public class MySqlConnectionImpl implements ConnectionManager{
    private Connection connection;
    private static final String USERNAME = "parham";
    private static final String PASSWORD = "123";

    public Connection getConnection() {
        try {
            connection = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/library?serverTimezone=UTC", USERNAME, PASSWORD);

        }catch (Exception e){
            System.out.println("ERROR : connection");
        }
        return connection;
    }
}
