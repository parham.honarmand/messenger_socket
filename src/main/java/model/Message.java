package model;

public class Message {
    private Integer id;
    private String message;
    private Integer userId;
    private boolean status = false;

    public Message(String message, Integer userId) {
        this.message = message;
        this.userId = userId;
    }

    public Message(Integer id, String message, Integer userId, boolean status) {
        this.id = id;
        this.message = message;
        this.userId = userId;
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Integer getId() {
        return id;
    }
}
