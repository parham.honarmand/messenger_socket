package utility;

import java.util.Scanner;

public class Utility {
    private static Utility utility;
    private Scanner scanner = new Scanner(System.in);

    private Utility(){}

    public static Utility getInstance(){
        if(utility == null)
            utility = new Utility();
        return utility;
    }

    public String getStringFromUser(){
        return scanner.nextLine();
    }

    public int getIntFromUser(){
        return Integer.valueOf(scanner.nextLine());
    }
}
