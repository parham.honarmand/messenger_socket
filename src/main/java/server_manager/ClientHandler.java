package server_manager;

import exception.NotFoundException;
import model.*;
import service.GeneralService;

import java.io.*;
import java.net.*;
import java.util.*;

public class ClientHandler extends Thread {
    private Socket socket;
    private ObjectInputStream objectInputStream;
    private ObjectOutputStream objectOutputStream;
    private DataInputStream dataInputStream;
    private DataOutputStream dataOutputStream;

    private User currentUser;

    ClientHandler(Socket socket) {
        this.socket = socket;
        try {
            objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
            objectInputStream = new ObjectInputStream(socket.getInputStream()) ;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        System.out.println("thread is started...");

        while (true) {
            try {
                objectOutputStream.writeObject("enter 1 for login or 2 for sign up :");
                objectOutputStream.flush();
                int input = (int) objectInputStream.readObject();

                switch (input) {
                    case 1:
                        login();
                        break;
                    case 2:
                        signUp();
                        login();
                        break;
                }

                List<String> messages = GeneralService.getUserService().getUserMessages(currentUser.getUsername());
                for (String s : messages) {
                    objectOutputStream.writeObject(currentUser.getUsername() + " : " + s);
                }

                while (true) {
                    objectOutputStream.writeObject("enter your destination username :");
                    String destinationUsername = (String) objectInputStream.readObject();

                    if (GeneralService.getUserService().checkforUser(destinationUsername))
                        break;
                }

                objectOutputStream.writeObject("enter your message :");
                String message = (String) objectInputStream.readObject();

                if (message.toLowerCase().equals("exit"))
                    break;

                Server.sendMessage(new Message(message, currentUser.getId()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            objectOutputStream.close();
            objectInputStream.close();
            dataInputStream.close();
            dataOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void signUp() {
        try {
            objectOutputStream.writeObject("enter your username :");
            String username = (String) objectInputStream.readObject();

            objectOutputStream.writeObject("enter your password");
            String password = (String) objectInputStream.readObject();

            GeneralService.getUserService().createUser(username, password, socket.getInetAddress().getHostAddress());
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void login() {
        try {
            objectOutputStream.writeObject("enter your username :");
            String username = (String) objectInputStream.readObject();

            if (!GeneralService.getUserService().checkforUser(username))
                login();

            objectOutputStream.writeObject("enter your password");
            String password = (String) objectInputStream.readObject();

            try {
                currentUser = GeneralService.getUserService().authentication(username, password);
            } catch (NotFoundException e) {
                login();
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
