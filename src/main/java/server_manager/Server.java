package server_manager;

import model.Message;
import service.GeneralService;

import java.io.*;
import java.net.*;
import java.util.*;

public class Server {
    static HashMap<String, Socket> sockets = new HashMap<>();
    static List<Thread> threads = new ArrayList<>();

    public static void main(String[] args) {
        ServerSocket serverSocket;
        Socket socket;

        try {
            serverSocket = new ServerSocket(5353);
            while (true) {
                socket = serverSocket.accept();
                System.out.println("client connected");
                sockets.put(String.valueOf(sockets.size()), socket);

                Thread thread = new ClientHandler(socket);
                threads.add(thread);
                thread.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static synchronized void sendMessage(Message message) {
        try {
            ObjectOutputStream objectOutputStream;
            for (String key : sockets.keySet()) {
                if (key.equals(message.getUserId())) {
                    objectOutputStream = new ObjectOutputStream(sockets.get(key).getOutputStream());
                    objectOutputStream.writeObject(message.getMessage());
                    objectOutputStream.flush();
                    message.setStatus(true);
                    System.out.println("message send");
                }
            }
            GeneralService.getMessageService().insert(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
