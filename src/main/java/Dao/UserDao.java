package Dao;

import connection_manager.*;
import exception.NotFoundException;
import model.User;

import java.sql.*;
import java.util.*;

public class UserDao {
    public ConnectionManager connectionManager = new MySqlConnectionImpl();
    private PreparedStatement statement;
    private static final String INSERT_QUERY = "insert into user (username, password, ip) +  value (?, ?, ?)";
    private static final String SEARCH_BY_ID_QUERY = "select * from user where user.id = ?";
    private static final String UPDATE_QUERY = "update user set username = ?, password = ?, ip = ? " +
            "where user.id = ?";
    private static final String DELETE_QUERY = "delete from user where user.id = ?";
    private static final String FIND_ALL_QUERY = "select * from user";
    private static final String SEARCH_BY_USERNAME_QUERY = "select * from user where user.username = ?";

    public void insert(User user) throws SQLException {
        statement = connectionManager.getConnection().prepareStatement(INSERT_QUERY);
        statement.setString(1, user.getUsername());
        statement.setString(2, user.getPassword());
        statement.setString(3, user.getIp());
        statement.executeUpdate();
    }

    public User searchById(int id) throws NotFoundException, SQLException {
        statement = connectionManager.getConnection().prepareStatement(SEARCH_BY_ID_QUERY);
        statement.setLong(1, id);
        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next())
            return new User(resultSet.getInt(1),
                    resultSet.getString(3), resultSet.getString(4),
                    resultSet.getString(5));

        throw new NotFoundException("this user does not exist");
    }

    public List<User> findAll() throws SQLException {
        List<User> userList = new ArrayList<>();
        statement = connectionManager.getConnection().prepareStatement(FIND_ALL_QUERY);
        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next())
            userList.add(new User(resultSet.getInt(1),
                    resultSet.getString(2), resultSet.getString(3),
                    resultSet.getString(4)));

        return userList;
    }

    public User searchByUsername(String username) throws NotFoundException, SQLException {
        statement = connectionManager.getConnection().prepareStatement(SEARCH_BY_USERNAME_QUERY);
        statement.setString(1, username);
        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next())
            return new User(resultSet.getInt(1),
                    resultSet.getString(2), resultSet.getString(3),
                    resultSet.getString(4));

        throw new NotFoundException("this user does not exist");
    }

    public void update(User user) throws SQLException {
        statement = connectionManager.getConnection().prepareStatement(UPDATE_QUERY);
        statement.setString(1, user.getUsername());
        statement.setString(2, user.getPassword());
        statement.setString(3, user.getIp());
        statement.setLong(4, user.getId());
        statement.executeUpdate();
    }

    public void delete(int id) throws SQLException {
        statement = connectionManager.getConnection().prepareStatement(DELETE_QUERY);
        statement.setLong(1, id);
        statement.executeUpdate();
    }
}
