package Dao;

import connection_manager.*;
import exception.NotFoundException;
import model.Message;

import java.sql.*;
import java.util.*;

public class GeneralDao {
    public ConnectionManager connectionManager = new MySqlConnectionImpl();
    private PreparedStatement statement;
    private static final String INSERT_QUERY = "insert into message (message, user_id, status) +  value (?, ?, ?)";
    private static final String SEARCH_BY_USER_ID_QUERY = "select * from message where message.user_id = ?";
    private static final String UPDATE_QUERY = "update message set message = ?, user_id = ?, status = ? " +
            "where message.id = ?";
    private static final String DELETE_QUERY = "delete from message where message.id = ?";

    public void insert(Message message) throws SQLException {
        statement = connectionManager.getConnection().prepareStatement(INSERT_QUERY);
        statement.setString(1, message.getMessage());
        statement.setLong(2, message.getUserId());
        statement.setString(3, (message.isStatus() ? "1" : "0"));
        statement.executeUpdate();
    }

    public List<Message> searchByUserId(int id) throws SQLException {
        List<Message> messages = new ArrayList<>();
        statement = connectionManager.getConnection().prepareStatement(SEARCH_BY_USER_ID_QUERY);
        statement.setLong(1, id);
        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next())
            messages.add(new Message(resultSet.getInt(1),
                    resultSet.getString(2), resultSet.getInt(3), resultSet.getBoolean(4)));

        return messages;
    }

    public void update(Message message) throws SQLException {
        statement = connectionManager.getConnection().prepareStatement(UPDATE_QUERY);
        statement.setString(1, message.getMessage());
        statement.setLong(2, message.getUserId());
        statement.setString(3, (message.isStatus() ? "1" : "0"));
        statement.setLong(3, message.getId());
        statement.executeUpdate();
    }

    public void delete(int id) throws SQLException {
        statement = connectionManager.getConnection().prepareStatement(DELETE_QUERY);
        statement.setLong(1, id);
        statement.executeUpdate();
    }
}
