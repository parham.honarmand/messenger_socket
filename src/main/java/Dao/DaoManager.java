package Dao;

public class DaoManager {
    private static UserDao userDao;
    private static GeneralDao messageDao;

    public static UserDao getUserDao() {
        if (userDao == null)
            userDao = new UserDao();
        return userDao;
    }

    public static GeneralDao getMessageDao() {
        if (messageDao == null)
            messageDao = new GeneralDao();
        return messageDao;
    }
}
