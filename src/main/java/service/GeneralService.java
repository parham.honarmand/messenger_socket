package service;

public class GeneralService {
    private static UserService userService;
    private static MessageService messageService;

    public static UserService getUserService(){
        if(userService == null)
            userService = new UserService();
        return userService;
    }

    public static MessageService getMessageService(){
        if(messageService == null)
            messageService = new MessageService();
        return messageService;
    }
}
