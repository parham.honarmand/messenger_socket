package service;

import Dao.*;
import model.Message;

import java.sql.SQLException;

public class MessageService {

    public void insert(Message message) {
        try {
            DaoManager.getMessageDao().insert(message);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
