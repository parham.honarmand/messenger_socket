package service;

import Dao.DaoManager;
import exception.NotFoundException;
import model.Message;
import model.User;

import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

public class UserService {
    public boolean checkforUser(String username) {
        try {
            User user = DaoManager.getUserDao().searchByUsername(username);
            if(user != null)
                return true;
        } catch (NotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public User authentication(String username, String password) throws NotFoundException {
        try {
            User user = DaoManager.getUserDao().searchByUsername(username);
            if(user != null)
                if(password.equals(user.getPassword()))
                    return user;
        } catch (NotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        throw new NotFoundException("user not founded");
    }

    public void createUser(String username, String password, String ip) {
        User user = new User(username, password, ip);
        try {
            DaoManager.getUserDao().insert(user);
        } catch (SQLException e) {
            System.out.println("can not add user");
        }
    }

    public List<String> getUserMessages(String username) throws NotFoundException {
        try {
            Integer userId = DaoManager.getUserDao().searchByUsername(username).getId();
            List<Message> messagesList = DaoManager.getMessageDao().searchByUserId(userId);
            List<String> messages = messagesList.stream().map(x -> x.getMessage())
                    .collect(Collectors.toList());
            for (Message message : messagesList) {
                message.setStatus(true);
                DaoManager.getMessageDao().update(message);
            }
            return messages;
        } catch (NotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        throw new NotFoundException("no message founded");
    }
}
