package client_manager;

import java.io.*;
import java.net.*;

public class MessageHandler extends Thread {
    private Socket socket;
    private ObjectInputStream objectInputStream;
    private ObjectOutputStream objectOutputStream;

    MessageHandler(Socket socket, ObjectOutputStream objectOutputStream) {
        this.socket = socket;
        this.objectOutputStream = objectOutputStream;
        try {
            objectInputStream = new ObjectInputStream(socket.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        while (true) {
            try {
                String receive = objectInputStream.readUTF();
                System.out.println("client is listening");
                System.out.println(receive);
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
