package client_manager;

import utility.Utility;

import java.io.*;
import java.net.*;

public class Client {

    public static void main(String[] args) throws UnknownHostException {
        Socket socket;
        ObjectOutputStream objectOutputStream;
        InetAddress ip = InetAddress.getByName("localhost");
        Integer port = 5353;


        try {
            socket = new Socket(ip, port);
            objectOutputStream = new ObjectOutputStream(socket.getOutputStream());

            Thread listener = new MessageHandler(socket, objectOutputStream);
            listener.start();

            while (true) {
                String toSend = Utility.getInstance().getStringFromUser();
                if (toSend.toLowerCase().equals("exit")) {
                    break;
                }
                objectOutputStream.writeObject(toSend);
                System.out.println("message was send");
            }
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
